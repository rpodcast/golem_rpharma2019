## Deployment to RStudio Connect

__Before Deployment__: Due to techinical issue, Create a text file called `.rsconnect_profile` in your application's directory (i.e. the root directory of your project) and add the following contents:

```
options(rsconnect.python.enabled = FALSE)
Sys.unsetenv("RETICULATE_PYTHON")
```

```r
rsconnect::deployApp(".",
  appFileManifest = "docs/app_manifest.txt",
  appPrimaryDoc = "index.html",
  appName = "golem_rpharma",
  appTitle = "Creating Shiny Apps with {golem}"
)
```
